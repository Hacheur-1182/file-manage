import express from "express";

import bodyParser from "body-parser";

import fs from "fs";

import multer from "multer";
import * as pdfjsLib from "pdfjs-dist";
import path from "path";
import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// const { nanoid } = require('nanoid');

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/uploads", express.static(path.join(__dirname, "/uploads")));

const filterFile = (req, file, cb) => {
  if (!file.originalname.match(/\.(pdf)$/)) {
    // Matches pdf files
    return cb(new Error("Please upload a pdf file"));
  }
  cb(undefined, true);
};

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads/");
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.originalname,
      // new Date().toISOString().replace(/:/g, "-") + "-" + file.originalname
    );
  },
});

const upload = multer({
  storage: fileStorage,
  limits: {
    fileSize: 1000000 * 10, // 10 MB
  },
  fileFilter: filterFile,
});

app.post("/upload", (req, res) => {
  console.log("body post");

  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
      return res.status(400).json({ error: err.message });
    } else if (err) {
      // An unknown error occurred when uploading.
      return res.status(422).json({ error: err.toString() });
    }
    console.log(req.file);
    // Everything went fine.
    fs.renameSync(req.file.path, req.file.destination + req.file.originalname);

    res.json({
      message: "File uploaded successfully",
      filename: req.file.originalname,
      path: req.file.path,
    });
  });
});

app.put("/update", upload.single("pdf"), (req, res) => {
  try {
    const { currentFilename } = req.body;
    console.log(currentFilename);
    fs.unlinkSync(currentFilename);
    // const uniqueName = nanoid() + ".pdf";
    fs.renameSync(req.file.path, req.file.destination + req.file.originalname);
    res.json({
      message: "File updated successfully",
      filename: req.file.originalname,
    });
  } catch (err) {
    res.status(500).json({ error: err.toString() });
  }
});

app.delete("/delete/:filename", (req, res) => {
  try {
    fs.unlinkSync("uploads/" + req.params.filename);
    res.json({ message: "File deleted successfully" });
  } catch (err) {
    res.status(500).json({ error: err.toString() });
  }
});

// PREVIEW

app.post("/preview", (req, res) => {
  try {
    const uniqueName = nanoid() + ".pdf";
    fs.renameSync(req.file.path, req.file.destination + uniqueName);

    // Read the PDF document
    pdfjsLib
      .getDocument({ url: req.file.destination + uniqueName })
      .promise.then((pdf) => {
        const numPages = pdf.numPages;
        res.json({
          message: "PDF previewed successfully",
          numPages: numPages,
          filename: uniqueName,
        });
      });
  } catch (err) {
    res.status(500).json({ error: err.toString() });
  }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
